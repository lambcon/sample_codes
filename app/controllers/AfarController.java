package controllers;

import actors.*;
import akka.NotUsed;
import akka.actor.Status;
import akka.stream.OverflowStrategy;
import akka.stream.javadsl.Source;
import akka.util.ByteString;
import au.com.bytecode.opencsv.CSVReader;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.avaje.ebean.Ebean;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.exc.UnrecognizedPropertyException;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.Sender;
import com.google.inject.Inject;
import com.mapped.publisher.actions.AdminAccess;
import com.mapped.publisher.actions.Authenticated;
import com.mapped.publisher.actions.Authorized;
import com.mapped.publisher.common.*;
import com.mapped.publisher.persistence.redis.RedisMgr;
import com.mapped.publisher.utils.*;
import com.mapped.publisher.view.*;
import com.umapped.external.afar.*;
import com.umapped.itinerary.analyze.model.Location;
import com.umapped.service.offer.*;
import it.innove.play.pdf.PdfGenerator;
import models.publisher.*;
import models.publisher.AfarCity;
import models.publisher.AfarCountry;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.NotImplementedException;
import org.apache.commons.lang3.time.StopWatch;
import org.apache.http.HttpResponse;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.h2.util.StringUtils;
import org.postgis.Point;
import play.data.DynamicForm;
import play.data.Form;
import play.data.FormFactory;
import play.libs.Json;
import play.libs.ws.WSClient;
import play.libs.ws.WSResponse;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.With;
import play.twirl.api.Html;

import java.util.*;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.ThreadLocalRandom;

import static play.db.DB.getConnection;
import play.libs.ws.*;

import com.umapped.external.afar.Destination;
import play.libs.concurrent.HttpExecutionContext;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.commons.lang3.time.StopWatch;

import com.google.inject.name.Named;

import javax.swing.text.html.HTMLDocument;

/**
 * Created by george on 2017-11-01.
 */
public class AfarController extends Controller {

  @Inject
  RedisMgr redis;

  @Inject
  FormFactory formFactory;

  @Inject
  private OfferService offerService;

  @Inject
  HttpExecutionContext ec;

  @Inject
  WSClient ws;

  @Inject private ObjectMapper objectMapper;

  static final int MAX_AFAR_POIS = 10;

  static int totalAfarCities;
  static int processedAfarCities;
  static int unprocessedAfarCities;
  static int updatedAfarCities;
  static int insertedAfarCities;

  static int totalAfarCountries;
  static int processedAfarCountries;
  static int unprocessedAfarCountries;
  static int updatedAfarCountries;
  static int insertedAfarCountries;

  static int totalAfarPois;
  static int processedAfarPois;
  static int unprocessedAfarPois;
  static int updatedAfarPois;
  static int insertedAfarPois;

  static int totalAfarDestinations;
  static int processedAfarDestinations;
  static int unprocessedAfarDestinations;

  static int totalAfarCityDest;
  static int processedAfarCityDest;
  static int unprocessedAfarCityDest;
  static int updatedAfarCityDest;
  static int insertedAfarCityDest;
  
  static int totalAfarCountryDest;
  static int processedAfarCountryDest;
  static int unprocessedAfarCountryDest;
  static int updatedAfarCountryDest;
  static int insertedAfarCountryDest;

  static int totalAfarCityLists;
  static int processedAfarCityLists;
  static int unprocessedAfarCityLists;
  static int updatedAfarCityLists;
  static int insertedAfarCityLists;

  static int totalAfarCountryLists;
  static int processedAfarCountryLists;
  static int unprocessedAfarCountryLists;
  static int updatedAfarCountryLists;
  static int insertedAfarCountryLists;

  static List<Integer> unprocessedDests;
  static List<Integer> unprocessedCityDests;
  static List<String> unprocessedLists = new ArrayList<>();
  static List<String> unprocessedPois;
  static List<String> unprocessedCountries;
  static List<String> unprocessedCities;
  static List<String> duplicatePois;

  
  static String afarMsg;

  @With({Authenticated.class, Authorized.class,  AdminAccess.class})
  public Result getAfar() {
    AfarInfoView view = new AfarInfoView();
    return ok(views.html.experimental.afarInfo.render(view));
  }

  @With({Authenticated.class, Authorized.class,  AdminAccess.class})
  public CompletionStage<Result> getAfarInfo() {
    totalAfarCities = 0;
    processedAfarCities = 0;
    unprocessedAfarCities = 0;
    updatedAfarCities = 0;
    insertedAfarCities = 0;

    totalAfarCountries = 0;
    processedAfarCountries = 0;
    unprocessedAfarCountries = 0;
    updatedAfarCountries = 0;
    insertedAfarCountries = 0;
    
    totalAfarPois = 0;
    processedAfarPois = 0;
    unprocessedAfarPois = 0;
    updatedAfarPois = 0;
    insertedAfarPois = 0;

    totalAfarCityDest = 0;
    processedAfarCityDest = 0;
    unprocessedAfarCityDest = 0;
    updatedAfarCityDest = 0;
    insertedAfarCityDest = 0;

    totalAfarCountryDest = 0;
    processedAfarCountryDest = 0;
    unprocessedAfarCountryDest = 0;
    updatedAfarCountryDest = 0;
    insertedAfarCountryDest = 0;

    totalAfarCityLists = 0;
    processedAfarCityLists = 0;
    unprocessedAfarCityLists = 0;
    updatedAfarCityLists = 0;
    insertedAfarCityLists = 0;

    totalAfarCountryLists = 0;
    processedAfarCountryLists = 0;
    unprocessedAfarCountryLists = 0;
    updatedAfarCountryLists = 0;
    insertedAfarCountryLists = 0;

    totalAfarDestinations = 0;
    processedAfarDestinations = 0;
    unprocessedAfarDestinations = 0;
    unprocessedDests = new ArrayList<>();
    unprocessedCityDests = new ArrayList<>();
    unprocessedPois = new ArrayList<>();
    unprocessedLists = new ArrayList<>();
    unprocessedCountries = new ArrayList<>();
    unprocessedCities = new ArrayList<>();
    duplicatePois = new ArrayList<>();

    afarMsg = null;

    AfarInfoView view = new AfarInfoView();
    Log.debug("AFAR Load: Begin Processing AFAR...");



    return CompletableFuture.supplyAsync(() -> {

          try {
            CookieStore httpCookieStore = new BasicCookieStore();
            HttpClient httpClient = HttpClientBuilder.create().setDefaultCookieStore(httpCookieStore).build();

            Log.debug("AFAR Load: Processing AFAR Countries...");
            AfarCountry country = AfarCountry.find.byId(0);
            if (country == null) {
              country = new AfarCountry();
              country.setAfarId(0);
              country.setName("No Country");
              country.setSlug("no-country");

            }
            Integer countryPage = 1;
            while (countryPage > 0) {
              CompletableFuture<Integer> countryPagePromise = getAfarCountries(countryPage);
              countryPage = countryPagePromise.get();
            }
            Log.debug("AFAR Load: Total Countries: "+totalAfarCountries);
            Log.debug("AFAR Load: Processed Countries: "+processedAfarCountries);
            Log.debug("AFAR Load: Inserted Countries: "+insertedAfarCountries);
            Log.debug("AFAR Load: Updated Countries: "+updatedAfarCountries);
            Log.debug("AFAR Load: Unprocessed Countries: "+unprocessedAfarCountries);


            Log.debug("Processing AFAR Cities...");
            AfarCity city = AfarCity.find.byId(0);
            if (city == null) {
              city = new AfarCity();
              city.setAfarId(0);
              city.setAfarCountryId(0);
              city.setName("No City");
              city.setSlug("no-city");

            }
            Integer cityPage = 1;
            while (cityPage > 0) {
              CompletableFuture<Integer> cityPagePromise = getAfarCities(cityPage);
              cityPage = cityPagePromise.get();
            }
            Log.debug("AFAR Load: Total Cities: "+totalAfarCities);
            Log.debug("AFAR Load: Processed Cities: "+processedAfarCities);
            Log.debug("AFAR Load: Inserted Cities: "+insertedAfarCities);
            Log.debug("AFAR Load: Updated Cities: "+updatedAfarCities);
            Log.debug("AFAR Load: Unprocessed Cities: "+unprocessedAfarCities);
            
            Log.debug("AFAR Load: Processing AFAR Destinations...");
            Integer destinationPage = 1;
            while (destinationPage > 0) {
              CompletableFuture<Integer> destinationPagePromise = getAfarDestinations(destinationPage);
              destinationPage = destinationPagePromise.get();
            }
            Log.debug("AFAR Load: Total City Dests: "+totalAfarCityDest);
            Log.debug("AFAR Load: Processed City Dests: "+processedAfarCityDest);
            Log.debug("AFAR Load: Inserted City Dests: "+insertedAfarCityDest);
            Log.debug("AFAR Load: Updated City Dests: "+updatedAfarCityDest);
            Log.debug("AFAR Load: Unprocessed City Dests: "+unprocessedAfarCityDest);
            Log.debug("AFAR Load: Unprocessed City Dests List:\n "+unprocessedCityDests.toString() +"\n");

            Log.debug("AFAR Load: Total Country Dests: "+totalAfarCountryDest);
            Log.debug("AFAR Load: Processed Country Dests: "+processedAfarCountryDest);
            Log.debug("AFAR Load: Inserted Country Dests: "+insertedAfarCountryDest);
            Log.debug("AFAR Load: Updated Country Dests: "+updatedAfarCountryDest);
            Log.debug("AFAR Load: Unprocessed Country Dests: "+unprocessedAfarCountryDest);

            Log.debug("AFAR Load: Total Destinations: "+totalAfarDestinations);
            Log.debug("AFAR Load: Unprocessed Destinations: "+unprocessedAfarDestinations);
            Log.debug("AFAR Load: Unprocessed Destinations List:\n "+ unprocessedDests.toString() +"\n");

            Log.debug("AFAR Load: Processing AFAR Lists...");
            Integer listPage = 1;
            while (listPage > 0) {
              listPage = getAfarList(listPage, httpClient);
            }
            Log.debug("AFAR Load: Total CityLists: "+totalAfarCityLists);
            Log.debug("AFAR Load: Processed CityLists: "+processedAfarCityLists);
            Log.debug("AFAR Load: Inserted CityLists: "+insertedAfarCityLists);
            Log.debug("AFAR Load: Updated CityLists: "+updatedAfarCityLists);
            Log.debug("AFAR Load: Unprocessed CityLists: "+unprocessedAfarCityLists);

            Log.debug("AFAR Load: Total CountryLists: "+totalAfarCountryLists);
            Log.debug("AFAR Load: Processed CountryLists: "+processedAfarCountryLists);
            Log.debug("AFAR Load: Inserted CountryLists: "+insertedAfarCountryLists);
            Log.debug("AFAR Load: Updated CountryLists: "+updatedAfarCountryLists);
            Log.debug("AFAR Load: Unprocessed CountryLists: "+unprocessedAfarCountryLists);

            Log.debug("AFAR Load: Total POIs: "+totalAfarPois);
            Log.debug("AFAR Load: Processed POIs: "+processedAfarPois);
            Log.debug("AFAR Load: Inserted POIs: "+insertedAfarPois);
            Log.debug("AFAR Load: Updated POIs: "+updatedAfarPois);
            Log.debug("AFAR Load: Unprocessed POIs: "+unprocessedAfarPois);
            Log.debug("AFAR Load: ===== Done Processing AFAR POIs =====\n");

            Log.debug("AFAR Load: Unprocessed Lists:\n "+unprocessedLists.toString() +"\n");
            Log.debug("AFAR Load: Unprocessed POIs:\n "+unprocessedPois.toString() +"\n");
            Log.debug("AFAR Load: Duplicate POIs:\n "+duplicatePois.toString() +"\n");


          }  catch (Exception e) {
            Log.err("Error Processing AFAR", e);
            afarMsg = "Error Processing AFAR";
          }

          if (totalAfarCountries > 0 || totalAfarCities > 0 || totalAfarCityDest > 0 || totalAfarPois > 0 || totalAfarCountryDest > 0
              || totalAfarCityLists > 0 || totalAfarCountryLists > 0) {
            afarMsg = "Successfully imported. Cities - [Total: " + totalAfarCities + "  Processed: " + processedAfarCities
                + "  Inserted: " + insertedAfarCities + "  Updated: " + updatedAfarCities
                + "  Unprocessed: " + unprocessedAfarCities + "]  Countries - [Total: " + totalAfarCountries + "  Processed: " + processedAfarCountries
                + "  Inserted: " + insertedAfarCountries + "  Updated: " + updatedAfarCountries
                + "  Unprocessed: " + unprocessedAfarCountries + "]  Destinations - [Total: " + totalAfarDestinations + "  Processed: " + processedAfarDestinations
                + "  Unprocessed: " + unprocessedAfarDestinations + "] Dest City - [Total: " + totalAfarCityDest + "  Processed: " + processedAfarCityDest
                + "  Inserted: " + insertedAfarCityDest + "  Updated: " + updatedAfarCityDest
                + "  Unprocessed: " + unprocessedAfarCityDest + "]  Dest Country - [Total: " + totalAfarCountryDest + "  Processed: " + processedAfarCountryDest
                + "  Inserted: " + insertedAfarCountryDest + "  Updated: " + updatedAfarCountryDest
                + "  Unprocessed: " + unprocessedAfarCountryDest + "] CityLists - [Total: " + totalAfarCityLists + "  Processed: " + processedAfarCityLists
                + "  Inserted: " + insertedAfarCityLists + "  Updated: " + updatedAfarCityLists
                + "  Unprocessed: " + unprocessedAfarCityLists + "] CountryLists - [Total: " + totalAfarCountryLists + "  Processed: " + processedAfarCountryLists
                + "  Inserted: " + insertedAfarCountryLists + "  Updated: " + updatedAfarCountryLists
                + "  Unprocessed: " + unprocessedAfarCountryLists + "] POIs - [Total: " + totalAfarPois + "  Processed: " + processedAfarPois
                + "  Inserted: " + insertedAfarPois + "  Updated: " + updatedAfarPois + "  Unprocessed: " + unprocessedAfarPois+"]";
          }

          view.message = afarMsg;
          Log.info("AFAR Load: " + afarMsg);
          return ok(views.html.experimental.afarInfo.render(view));

        }, ec.current()).exceptionally((Throwable throwable) -> {
          Log.err("AFAR: Exception on processing Cities: " + throwable.getMessage());
          view.message = "AFAR: Exception on processing Cities";
          return ok(views.html.experimental.afarInfo.render(view));
        });

  }

  public CompletableFuture<Integer> getAfarDestinations(Integer inPageNum) {
    String destUrl = "https://afarUrl" + inPageNum;

    WSRequest request = ws.url(destUrl).setRequestTimeout(120000L);

    CompletionStage<WSResponse> respPromise = request.get();

    CompletionStage<Integer> destResult = respPromise.thenApplyAsync((WSResponse response) -> {
      //Run code for POI
      JsonNode json = response.asJson();
      try {
        ObjectMapper mapper = new ObjectMapper();
        ObjectReader r = mapper.readerFor(DestinationRoot.class);
        DestinationRoot destinationRoot = r.readValue(json);

        if (destinationRoot == null || destinationRoot.getDestinations() == null || destinationRoot.getDestinations().size() < 1) {
          Log.debug("Empty page - Reached end of AFAR Destination pages.");
          return 0;
        }

        Log.debug("Processing AFAR Destination page: "+inPageNum);
        

        for(Destination d : destinationRoot.getDestinations()) {
          if (d != null) {
            d.setOverview(d.getOverview().replace("\u0000", ""));
            if (d.getCityId() != null && !d.getCityId().equals("null")
                && (d.getCountryId() == null || d.getCountryId().equals("null"))) {
              CompletableFuture<com.umapped.external.afar.AfarCity> cityPromise = getAfarCity(d.getCityId());
              com.umapped.external.afar.AfarCity afarCity = cityPromise.get();

              processAfarCity(d, afarCity);
              processedAfarDestinations++;

            } else if (d.getCountryId() != null && !d.getCountryId().equals("null")
                && (d.getCityId() == null || d.getCityId().equals("null"))) {

              processAfarCountry(d);
              processedAfarDestinations++;

            } else if (d.getCountryId() == null && d.getCityId() == null && d.getRegionId() == null & d.getTagId() == null){
              unprocessedAfarDestinations++;
              unprocessedDests.add(d.getId());
            } else {
              unprocessedAfarDestinations++;
            }
            totalAfarDestinations++;
          }
        }
        return inPageNum + 1;
      }  catch (Exception e) {
        Log.err("Error Processing AFAR Destinations ", e);
        afarMsg = "Error Processing AFAR Destinations - page " + inPageNum;
        return inPageNum + 1;
      }
      
    }, ec.current()).exceptionally((Throwable throwable) -> {
      Log.err("AFAR: Exception on processing Destinations: " + throwable.getMessage());
      afarMsg = "AFAR: Exception on processing Destinations. ";
      return null;
    });
    return destResult.toCompletableFuture();
  }



  public Integer getAfarList(Integer inPageNum, HttpClient httpClient1) {
    String poiUrl = "afarUrl" + inPageNum;

    try {
      CookieStore httpCookieStore = new BasicCookieStore();
      HttpClient httpClient = HttpClientBuilder.create().setDefaultCookieStore(httpCookieStore).build();

      HttpGet httpGet = new HttpGet(poiUrl);

      httpGet.setHeader("Accept","application/json, text/plain, */*");

      HttpResponse r1 = httpClient.execute(httpGet);

      if (r1 != null && r1.getStatusLine() != null && r1.getStatusLine().getStatusCode() < 300 ) {

        ObjectMapper mapper = new ObjectMapper();
        JsonNode json  = mapper.readTree(EntityUtils.toString(r1.getEntity()));

        ObjectReader r = mapper.readerFor(AfarListRoot.class);
        AfarListRoot afarListRoot = r.readValue(json);

        if (afarListRoot == null || afarListRoot.getAfarLists() == null || afarListRoot.getAfarLists().size() < 1) {
          Log.debug("Empty page - Reached end of AFAR List pages.");
          return 0;
        }

        Log.debug("Processing AFAR List page: " + inPageNum);

        for (AfarList afarList : afarListRoot.getAfarLists()) {
          if (afarList.getCityId() != null && !afarList.getCityId()
                                                       .equals("null") && (afarList.getCountryId() == null || afarList.getCountryId()

                                                                                                                      .equals(
                                                                                                                          "null"))) {
            processAfarCityList(afarList);

          }
          else if (afarList.getCountryId() != null && !afarList.getCountryId()
                                                               .equals("null") && (afarList.getCityId() == null ||
                                                                                   afarList
              .getCityId()
              .equals("null"))) {
            processAfarCountryList(afarList);

          }
          else {
            StringBuilder sb = new StringBuilder();
            sb.append("Others");
            sb.append("-");
            sb.append(afarList.getId());
            if (afarList.getSlug() != null && !afarList.getSlug().isEmpty()) {
              sb.append("-");
              sb.append(afarList.getSlug());
            }
            else if (afarList.getTitle() != null && !afarList.getTitle().isEmpty()) {
              sb.append("-");
              sb.append(afarList.getTitle());
            }
            unprocessedLists.add(sb.toString());
          }

        }
      }

      return inPageNum + 1;
    } catch (Exception e) {
      Log.err("Error Processing AFAR Lists ", e);
      afarMsg = "Error Processing AFAR Lists " + inPageNum;
      return inPageNum + 1;
    }

/*

    WSRequest request = ws.url(poiUrl).setRequestTimeout(120000L);

    CompletionStage<WSResponse> respPromise = request.get();

    CompletionStage<Integer> poiResult = respPromise.thenApplyAsync((WSResponse response) -> {
      //Run code for POI
      JsonNode json = response.asJson();
      try {
        ObjectMapper mapper = new ObjectMapper();
        ObjectReader r = mapper.readerFor(AfarListRoot.class);
        AfarListRoot afarListRoot = r.readValue(json);

        if (afarListRoot == null || afarListRoot.getAfarLists() == null || afarListRoot.getAfarLists().size() < 1) {
          Log.debug("Empty page - Reached end of AFAR List pages.");
          return 0;
        }

        Log.debug("Processing AFAR List page: "+inPageNum);

        for(AfarList afarList : afarListRoot.getAfarLists()) {
          if (afarList.getCityId() != null && !afarList.getCityId().equals("null")
              && (afarList.getCountryId() == null || afarList.getCountryId().equals("null"))) {
            processAfarCityList(afarList);

          } else if (afarList.getCountryId() != null && !afarList.getCountryId().equals("null")
              && (afarList.getCityId() == null || afarList.getCityId().equals("null"))) {
            processAfarCountryList(afarList);

          } else {
            StringBuilder sb = new StringBuilder();
            sb.append("Others");
            sb.append("-");
            sb.append(afarList.getId());
            if (afarList.getSlug() != null && !afarList.getSlug().isEmpty()) {
              sb.append("-");
              sb.append(afarList.getSlug());
            } else if (afarList.getTitle() != null && !afarList.getTitle().isEmpty()) {
              sb.append("-");
              sb.append(afarList.getTitle());
            }
            unprocessedLists.add(sb.toString());
          }

        }

        return inPageNum + 1;
      } catch (Exception e) {
        Log.err("Error Processing AFAR Lists ", e);
        afarMsg = "Error Processing AFAR Lists " + inPageNum;
        return inPageNum + 1;
      }
    }, ec.current()).exceptionally((Throwable throwable) -> {
      Log.err("AFAR: Exception on processing Lists: " + throwable.getMessage());
      afarMsg = "AFAR: Exception on processing Lists: ";
      return null;
    });
    return poiResult.toCompletableFuture();
    */
  }
  

  public CompletableFuture<Integer> getAfarCountries(Integer inPageNum) {
    String poiUrl = "afarUrl" + inPageNum;

    WSRequest request = ws.url(poiUrl).setRequestTimeout(120000L);

    CompletionStage<WSResponse> respPromise = request.get();

    CompletionStage<Integer> countryResult = respPromise.thenApplyAsync((WSResponse response) -> {
      //Run code for POI
      JsonNode json = response.asJson();
      try {
        ObjectMapper mapper = new ObjectMapper();
        ObjectReader r = mapper.readerFor(AfarCountryRoot.class);
        AfarCountryRoot afarCountryRoot = r.readValue(json);

        if (afarCountryRoot == null || afarCountryRoot.getCountries() == null || afarCountryRoot.getCountries().size() < 1) {
          Log.debug("Empty page - Reached end of AFAR Countries pages.");
          return 0;
        }

        Log.debug("Processing AFAR Countries page: "+inPageNum);

        for(com.umapped.external.afar.AfarCountry afarCountry : afarCountryRoot.getCountries()) {
          if (afarCountry.getAfarId() != null && !afarCountry.getAfarId().equals("null")) {
            AfarCountry country = AfarCountry.find.byId(afarCountry.getAfarId());
            if (country == null) {
              country = new AfarCountry();
              country.setAfarId(afarCountry.getAfarId());
              country.setName(afarCountry.getName());
              country.setSlug(afarCountry.getSlug());
              if (afarCountry.getLng() != null && afarCountry.getLat()!= null) {
                try {
                  country.setLocation(new Point(Double.parseDouble(afarCountry.getLat()), Double.parseDouble(afarCountry.getLng())));
                } catch (NumberFormatException e) {
                  Log.debug("Incorrect Lat/Long for Country Id: "+afarCountry.getAfarId());
                } catch (Exception e) {
                  Log.debug("Incorrect Lat/Long for Country Id: "+afarCountry.getAfarId());
                }
              }
              country.insert();
              insertedAfarCountries++;
            } else {
              country.setName(afarCountry.getName());
              country.setSlug(afarCountry.getSlug());
              if (afarCountry.getLng() != null && afarCountry.getLat()!= null) {
                try {
                  country.setLocation(new Point(Double.parseDouble(afarCountry.getLat()), Double.parseDouble(afarCountry.getLng())));
                } catch (NumberFormatException e) {
                  Log.debug("Incorrect Lat/Long for Country Id: "+afarCountry.getAfarId());
                } catch (Exception e) {
                  Log.debug("Incorrect Lat/Long for Country Id: "+afarCountry.getAfarId());
                }
              }
              country.update();
              updatedAfarCountries++;
            }
            processedAfarCountries++;
          } else {
            StringBuilder sb = new StringBuilder();
            sb.append("Country");
            sb.append("-");
            if (afarCountry.getSlug() != null && !afarCountry.getSlug().isEmpty()) {
              sb.append("-");
              sb.append(afarCountry.getSlug());
            } else if (afarCountry.getName() != null && !afarCountry.getName().isEmpty()) {
              sb.append("-");
              sb.append(afarCountry.getName());
            }
            unprocessedAfarCountries++;
            unprocessedCountries.add(sb.toString());
          }
          totalAfarCountries++;

        }

        return inPageNum + 1;
      } catch (Exception e) {
        Log.err("Error Processing AFAR Countries ", e);
        afarMsg = "Error Processing AFAR Countries " + inPageNum;
        return inPageNum + 1;
      }
    }, ec.current()).exceptionally((Throwable throwable) -> {
      Log.err("AFAR: Exception on processing Countries: " + throwable.getMessage());
      afarMsg = "AFAR: Exception on processing Countries: ";
      return null;
    });
    return countryResult.toCompletableFuture();
  }


  public CompletableFuture<Integer> getAfarCities(Integer inPageNum) {
    String poiUrl = "afarUrl" + inPageNum;

    WSRequest request = ws.url(poiUrl).setRequestTimeout(120000L);

    CompletionStage<WSResponse> respPromise = request.get();

    CompletionStage<Integer> cityResult = respPromise.thenApplyAsync((WSResponse response) -> {
      //Run code for POI
      JsonNode json = response.asJson();
      try {
        ObjectMapper mapper = new ObjectMapper();
        ObjectReader r = mapper.readerFor(AfarCityRoot.class);
        AfarCityRoot afarCityRoot = r.readValue(json);

        if (afarCityRoot == null || afarCityRoot.getAfarCity() == null || afarCityRoot.getAfarCity().size() < 1) {
          Log.debug("Empty page - Reached end of AFAR Cities pages.");
          return 0;
        }

        Log.debug("Processing AFAR Cities page: "+inPageNum);

        UMappedCityLookup umCity = new UMappedCityLookup();
        for(com.umapped.external.afar.AfarCity afarCity : afarCityRoot.getAfarCity()) {
          UMappedCity umc = null;
          if (afarCity != null && afarCity.getLng() != null && afarCity.getLat() != null) {
            try {
              com.umapped.itinerary.analyze.model.Location.GeoLocation loc = new Location.GeoLocation(Double.parseDouble(afarCity.getLat()), Double.parseDouble(afarCity.getLng()));
              umc = umCity.getCity(loc);
            } catch (NumberFormatException e) {
              Log.debug("Incorrect Lat/Long for City Id: " + afarCity.getAfarId());
            } catch (Exception e) {
              Log.debug("Incorrect Lat/Long for City Id: " + afarCity.getAfarId());
            }
          }

          if (afarCity.getAfarId() != null && !afarCity.getAfarId().equals("null")) {
            AfarCity city = AfarCity.find.byId(afarCity.getAfarId());
            if (city == null) {
              city = new AfarCity();
              city.setAfarId(afarCity.getAfarId());
              city.setAfarCountryId(afarCity.getCountryId());
              if (umc != null && umc.getId() != null) {
                city.setUmCityId(umc.getId());
              }
              city.setName(afarCity.getName());
              city.setSlug(afarCity.getSlug());
              if (afarCity.getLng() != null && afarCity.getLat() != null) {
                try {
                  city.setLocation(new Point(Double.parseDouble(afarCity.getLng()), Double.parseDouble(afarCity.getLat())));
                } catch (NumberFormatException e) {
                  Log.debug("Incorrect Lat/Long for City Id: " + afarCity.getAfarId());
                } catch (Exception e) {
                  Log.debug("Incorrect Lat/Long for City Id: " + afarCity.getAfarId());
                }
              }
              city.insert();
              insertedAfarCities++;
            } else {
              city.setName(afarCity.getName());
              city.setSlug(afarCity.getSlug());
              if (umc != null && umc.getId() != null) {
                city.setUmCityId(umc.getId());
              }
              if (afarCity.getLng() != null && afarCity.getLat() != null) {
                try {
                  city.setLocation(new Point(Double.parseDouble(afarCity.getLng()), Double.parseDouble(afarCity.getLat())));
                } catch (NumberFormatException e) {
                  Log.debug("Incorrect Lat/Long for City Id: " + afarCity.getAfarId());
                } catch (Exception e) {
                  Log.debug("Incorrect Lat/Long for City Id: " + afarCity.getAfarId());
                }
              }
              city.update();
              updatedAfarCities++;
            }
            processedAfarCities++;
          } else {
            StringBuilder sb = new StringBuilder();
            sb.append("City");
            sb.append("-");
            if (afarCity.getSlug() != null && !afarCity.getSlug().isEmpty()) {
              sb.append("-");
              sb.append(afarCity.getSlug());
            } else if (afarCity.getName() != null && !afarCity.getName().isEmpty()) {
              sb.append("-");
              sb.append(afarCity.getName());
            }
            unprocessedAfarCities++;
            unprocessedCities.add(sb.toString());
          }

          totalAfarCities++;
        }

        return inPageNum + 1;
      } catch (Exception e) {
        Log.err("Error Processing AFAR Cities ", e);
        afarMsg = "Error Processing AFAR Cities " + inPageNum;
        return inPageNum + 1;
      }
    }, ec.current()).exceptionally((Throwable throwable) -> {
      Log.err("AFAR: Exception on processing Cities: " + throwable.getMessage());
      afarMsg = "AFAR: Exception on processing Cities: ";
      return null;
    });
    return cityResult.toCompletableFuture();
  }


  public CompletableFuture<com.umapped.external.afar.AfarCity> getAfarCity(Integer cityId) {
    String cityUrl = "afarUrl" + cityId;

    WSRequest request = ws.url(cityUrl).setRequestTimeout(120000L);

    CompletionStage<WSResponse> respPromise = request.get();

    CompletionStage<com.umapped.external.afar.AfarCity> cityResult = respPromise.thenApply((WSResponse response) -> {
      JsonNode json = response.asJson();
      try {
        ObjectMapper mapper = new ObjectMapper();
        ObjectReader r = mapper.readerFor(com.umapped.external.afar.AfarCity.class);
        com.umapped.external.afar.AfarCity afarCity = r.readValue(json);

        if (afarCity == null) {
          Log.debug("Empty page - City does not exist");
          return null;
        } else {
          return afarCity;
        }
      } catch (Exception e) {
        Log.err("Error retrieving AFAR City ", e);
        return null;
      }
    }).exceptionally((Throwable throwable) -> {
      Log.err("AFAR: Exception on retrieving City: " + throwable.getMessage());
      return null;
    });
    return cityResult.toCompletableFuture();
  }
  

  public static void processAfarCountry(Destination d) {
    try {
      //Process all partial List items and create IdList object first
      IdList idList = null;
      if (d.getLists() != null && d.getLists().size() > 0) {
        idList = new IdList();
        List<Integer> ids = new ArrayList<>();
        for (AfarList list : d.getLists()) {
          if (list.getId() != null) {
            AfarCountryList afarCountryList = AfarCountryList.find.byId(list.getId());
            if (afarCountryList != null) {
              afarCountryList.setSlug(list.getSlug());
              afarCountryList.setAfarCountryId(d.getCountryId());
              afarCountryList.setAfarDestId(d.getId());
              afarCountryList.update();
              updatedAfarCountryLists++;
            } else {
              afarCountryList = new AfarCountryList();
              afarCountryList.setAfarId(list.getId());
              afarCountryList.setSlug(list.getSlug());
              afarCountryList.setAfarCountryId(d.getCountryId());
              afarCountryList.setAfarDestId(d.getId());
              afarCountryList.insert();
              insertedAfarCountryLists++;
            }
            processedAfarCountryLists++;
            ids.add(list.getId());
          } else {
            unprocessedAfarCountryLists++;
            StringBuilder sb = new StringBuilder();
            sb.append("CountryList");
            sb.append("-");
            sb.append(d.getId());
            if (list.getSlug() != null && !list.getSlug().isEmpty()) {
              sb.append("-");
              sb.append(list.getSlug());
            }
            unprocessedLists.add(sb.toString());
          }
          totalAfarCountryLists++;
        }
        idList.setIds(ids);
      }

      AfarCountry ac = AfarCountry.find.byId(d.getCountryId());
      if (ac != null) {
        ac.setAfarDestId(d.getId());
        ac.setDescription(d.getOverview());
        ac.setImageUrl(d.getHeroImageUrl());
        ac.setPhotoCredit(d.getPhotoCredit());
        ac.setCountryLists(ac.marshalCountryLists(idList));
        ac.setData(ac.marshalData(d));
        ac.update();
        updatedAfarCountryDest++;
      } else {
        ac = new AfarCountry();
        ac.setAfarId(d.getCountryId());
        ac.setAfarDestId(d.getId());
        ac.setName(d.getName());
        ac.setDescription(d.getOverview());
        ac.setImageUrl(d.getHeroImageUrl());
        ac.setPhotoCredit(d.getPhotoCredit());
        ac.setCountryLists(ac.marshalCountryLists(idList));
        ac.setData(ac.marshalData(d));
        ac.insert();
        insertedAfarCountryDest++;
      }
      processedAfarCountryDest++;
      totalAfarCountryDest++;
    }
    catch(Exception e) {
      Log.err("Error Processing AFAR Country ", e);
    }
  }

  
  public static void processAfarCity(Destination d, com.umapped.external.afar.AfarCity afarCity) {
    UMappedCityLookup umCity = new UMappedCityLookup();
    try {
      UMappedCity umc = null;
      if (afarCity != null && afarCity.getLng() != null && afarCity.getLat() != null) {
        try {
          com.umapped.itinerary.analyze.model.Location.GeoLocation loc = new Location.GeoLocation(Double.parseDouble(afarCity.getLat()), Double.parseDouble(afarCity.getLng()));
          umc = umCity.getCity(loc);
        } catch (NumberFormatException e) {
          Log.debug("Incorrect Lat/Long for City Id: " + afarCity.getAfarId());
        } catch (Exception e) {
          Log.debug("Incorrect Lat/Long for City Id: " + afarCity.getAfarId());
        }
      }
      if (afarCity != null) {
        //Process all partial List items and create IdList object
        IdList idList = null;
        if (d.getLists() != null && d.getLists().size() > 0) {
          idList = new IdList();
          List<Integer> ids = new ArrayList<>();
          for (AfarList list : d.getLists()) {
            if (list.getId() != null) {
              AfarCityList afarCityList = AfarCityList.find.byId(list.getId());
              if (afarCityList == null) {
                afarCityList = new AfarCityList();
                afarCityList.setAfarId(list.getId());
                afarCityList.setSlug(list.getSlug());
                afarCityList.setAfarCityId(d.getCityId());
                afarCityList.setAfarDestId(d.getId());
                afarCityList.insert();
                insertedAfarCityLists++;
              } else {
                afarCityList.setSlug(list.getSlug());
                afarCityList.setAfarCityId(d.getCityId());
                afarCityList.setAfarDestId(d.getId());
                afarCityList.update();
                updatedAfarCityLists++;
              }
              processedAfarCityLists++;
              ids.add(list.getId());
            } else {
              unprocessedAfarCityLists++;
              StringBuilder sb = new StringBuilder();
              sb.append("CityList");
              sb.append("-");
              sb.append(d.getId());
              if (list.getSlug() != null && !list.getSlug().isEmpty()) {
                sb.append("-");
                sb.append(list.getSlug());
              }
              unprocessedLists.add(sb.toString());
            }
            totalAfarCityLists++;
          }
          idList.setIds(ids);
        }

        AfarCity ac = AfarCity.find.byId(d.getCityId());
        if (ac != null) {
          ac.setAfarDestId(d.getId());
          ac.setDescription(d.getOverview());
          ac.setImageUrl(d.getHeroImageUrl());
          ac.setPhotoCredit(d.getPhotoCredit());
          ac.setCityLists(ac.marshalCityLists(idList));
          ac.setData(ac.marshalData(d));
          ac.update();
          updatedAfarCityDest++;
        } else {
          ac = new AfarCity();
          ac.setAfarId(d.getCityId());
          ac.setAfarCountryId(afarCity.getCountryId());
          if (umc != null && umc.getId() != null) {
            ac.setUmCityId(umc.getId());
          }
          ac.setAfarDestId(d.getId());
          ac.setName(d.getName());
          ac.setDescription(d.getOverview());
          ac.setImageUrl(d.getHeroImageUrl());
          ac.setPhotoCredit(d.getPhotoCredit());
          if (afarCity.getLng() != null && afarCity.getLat() != null) {
            try {
              ac.setLocation(new Point(Double.parseDouble(afarCity.getLat()), Double.parseDouble(afarCity.getLng())));
            } catch (NumberFormatException e) {
              Log.debug("Incorrect Lat/Long for City Id: " + afarCity.getAfarId());
            } catch (Exception e) {
              Log.debug("Incorrect Lat/Long for City Id: " + afarCity.getAfarId());
            }
          }
          ac.setCityLists(ac.marshalCityLists(idList));
          ac.setData(ac.marshalData(d));
          ac.insert();
          insertedAfarCityDest++;
        }
        processedAfarCityDest++;
      } else {
        unprocessedAfarCityDest++;
        unprocessedCityDests.add(d.getId());
      }
      totalAfarCityDest++;
    }
    catch(Exception e) {
      Log.err("Error Processing AFAR City ", e);
    }
  }
  

  public static void processAfarCityList(AfarList afarList) {
    try {
      //Process all partial List items and create IdList object first
      IdList idList = null;
      if (afarList.getHighlights() != null && afarList.getHighlights().size() > 0) {
        idList = new IdList();
        List<Integer> ids = new ArrayList<>();
        List<Highlight> sanitizedHighlights = new ArrayList<>();
        for (Highlight highlight : afarList.getHighlights()) {

          //HACK: to sanitize description and remove invalid unicodes
          String tempDesc = highlight.getDescription().replace("\u0000", "");
          highlight.setDescription(tempDesc);
          sanitizedHighlights.add(highlight);
          ids.add(highlight.getId());

          if (highlight.getId() != null && !highlight.getId().equals("null")
              && highlight.getCityId() != null && !highlight.getCityId().equals("null")
              && highlight.getCountryId() != null && !highlight.getCountryId().equals("null")) {

            processAfarPoi(highlight, afarList.getId());

          } else {
            unprocessedAfarPois++;
            StringBuilder sb = new StringBuilder();
            sb.append(afarList.getId());
            if (highlight.getSlug() != null && !highlight.getSlug().isEmpty()) {
              sb.append("-");
              sb.append(highlight.getSlug());
            } else if (highlight.getTitle() != null && !highlight.getTitle().isEmpty()) {
              sb.append("-");
              sb.append(highlight.getTitle());
            }
            unprocessedPois.add(sb.toString());
          }
          totalAfarPois++;
        }
        afarList.setHighlights(sanitizedHighlights);
        idList.setIds(ids);
      }
      
      if (afarList.getId() != null) {
        AfarCityList acl = AfarCityList.find.byId(afarList.getId());
        if (acl != null) {
          acl.setAfarDestId(afarList.getId());
          acl.setName(afarList.getTitle());
          acl.setSlug(afarList.getSlug());
          acl.setType(afarList.getCategoryType());
          if (idList != null) {
            acl.setPois(acl.marshalPois(idList));
          }
          acl.setData(acl.marshalData(afarList));
          acl.update();
          updatedAfarCityLists++;
        } else {
          acl = new AfarCityList();
          acl.setAfarId(afarList.getId());
          acl.setAfarCityId(afarList.getCityId());
          acl.setAfarDestId(afarList.getDestinationId());
          acl.setName(afarList.getTitle());
          acl.setSlug(afarList.getSlug());
          acl.setType(afarList.getCategoryType());
          if (idList != null) {
            acl.setPois(acl.marshalPois(idList));
          }
          acl.setData(acl.marshalData(afarList));
          acl.insert();
          insertedAfarCityLists++;
        }
        processedAfarCityLists++;
      } else {
        unprocessedAfarCityLists++;
        StringBuilder sb = new StringBuilder();
        sb.append("CityList");
        sb.append("-");
        sb.append(afarList.getDestinationId());
        if (afarList.getSlug() != null && !afarList.getSlug().isEmpty()) {
          sb.append("-");
          sb.append(afarList.getSlug());
        } else if (afarList.getTitle() != null && !afarList.getTitle().isEmpty()) {
          sb.append("-");
          sb.append(afarList.getTitle());
        }
        unprocessedLists.add(sb.toString());
      }
      totalAfarCityLists++;
    }
    catch(Exception e) {
      Log.err("Error Processing AFAR City List ", e);
    }
  }
  

  public static void processAfarCountryList(AfarList afarList) {
    try {
      //Process all partial List items and create IdList object first
      IdList idList = null;
      if (afarList.getHighlights() != null && afarList.getHighlights().size() > 0) {
        idList = new IdList();
        List<Integer> ids = new ArrayList<>();
        List<Highlight> sanitizedHighlights = new ArrayList<>();
        for (Highlight highlight : afarList.getHighlights()) {

          //HACK: to sanitize description and remove invalid unicodes
          String tempDesc = highlight.getDescription().replace("\u0000", "");
          highlight.setDescription(tempDesc);
          sanitizedHighlights.add(highlight);
          ids.add(highlight.getId());

          if (highlight.getId() != null && !highlight.getId().equals("null")
              && highlight.getCityId() != null && !highlight.getCityId().equals("null")
              && highlight.getCountryId() != null && !highlight.getCountryId().equals("null")) {

            processAfarPoi(highlight, afarList.getId());

          } else {
            unprocessedAfarPois++;
            StringBuilder sb = new StringBuilder();
            sb.append(afarList.getId());
            if (highlight.getSlug() != null && !highlight.getSlug().isEmpty()) {
              sb.append("-");
              sb.append(highlight.getSlug());
            } else if (highlight.getTitle() != null && !highlight.getTitle().isEmpty()) {
              sb.append("-");
              sb.append(highlight.getTitle());
            }
            unprocessedPois.add(sb.toString());
          }
          totalAfarPois++;
        }
        afarList.setHighlights(sanitizedHighlights);
        idList.setIds(ids);
      }

      if (afarList.getId() != null) {
        AfarCountryList acl = AfarCountryList.find.byId(afarList.getId());
        if (acl != null) {
          acl.setAfarDestId(afarList.getDestinationId());
          acl.setName(afarList.getTitle());
          acl.setSlug(afarList.getSlug());
          acl.setType(afarList.getCategoryType());
          if (idList != null) {
            acl.setPois(acl.marshalPois(idList));
          }
          acl.setData(acl.marshalData(afarList));
          acl.update();
          updatedAfarCountryLists++;
        } else {
          acl = new AfarCountryList();
          acl.setAfarId(afarList.getId());
          acl.setAfarCountryId(afarList.getCountryId());
          acl.setAfarDestId(afarList.getDestinationId());
          acl.setName(afarList.getTitle());
          acl.setSlug(afarList.getSlug());
          acl.setType(afarList.getCategoryType());
          if (idList != null) {
            acl.setPois(acl.marshalPois(idList));
          }
          acl.setData(acl.marshalData(afarList));
          acl.insert();
          insertedAfarCountryLists++;
        }
        processedAfarCountryLists++;
      } else {
        unprocessedAfarCountryLists++;
        StringBuilder sb = new StringBuilder();
        sb.append("CountryList");
        sb.append("-");
        sb.append(afarList.getDestinationId());
        if (afarList.getSlug() != null && !afarList.getSlug().isEmpty()) {
          sb.append("-");
          sb.append(afarList.getSlug());
        } else if (afarList.getTitle() != null && !afarList.getTitle().isEmpty()) {
          sb.append("-");
          sb.append(afarList.getTitle());
        }
        unprocessedLists.add(sb.toString());
      }
      totalAfarCountryLists++;
    }
    catch(Exception e) {
      Log.err("Error Processing AFAR Country List ", e);
    }
  }


  public static void processAfarPoi(Highlight highlight, Integer listId) {
    try {
      CategoryList categories = null;
      if (highlight.getCategories() != null && highlight.getCategories().size() > 0) {
        categories = new CategoryList();
        List<String> list = new ArrayList<>();
        for (String cat : highlight.getCategories()) {
          list.add(cat);
        }
        categories.setCategories(list);
      }

      AfarPoi poi = AfarPoi.find.byId(highlight.getId());

      if (poi != null) {
        poi.setTitle(highlight.getTitle());
        poi.setSlug(highlight.getSlug());
        poi.setPlaceName(highlight.getPlaceName());
        if (categories != null) {
          poi.setTypes(poi.marshalTypes(categories));
        }
        poi.setDescription(highlight.getDescription().replace("\u0000", ""));
        if (highlight.getLongitude() != null && highlight.getLatitude() != null) {
          try {
            poi.setLocation(new Point(Double.parseDouble(highlight.getLongitude()), Double.parseDouble(highlight.getLatitude())));
          } catch (NumberFormatException e) {
            Log.debug("Incorrect Lat/Long for Poi Id: "+ highlight.getId());
          } catch (Exception e) {
            Log.debug("Incorrect Lat/Long for Poi Id: "+ highlight.getId());
          }
        }
        poi.setData(poi.marshalData(highlight));
        poi.setAfarCountryId(highlight.getCountryId());
        poi.setAfarListId(listId);
        poi.update();
        updatedAfarPois++;
        StringBuilder sb = new StringBuilder();
        sb.append(highlight.getId());
        sb.append("-");
        sb.append(listId);
        duplicatePois.add(sb.toString());

      } else {
        poi = new AfarPoi();
        poi.setAfarId(highlight.getId());
        poi.setAfarCityId(highlight.getCityId());
        poi.setAfarCountryId(highlight.getCountryId());
        poi.setAfarListId(listId);
        poi.setTitle(highlight.getTitle());
        poi.setSlug(highlight.getSlug());
        poi.setPlaceName(highlight.getPlaceName());
        if (categories != null) {
          poi.setTypes(poi.marshalTypes(categories));
        }
        poi.setDescription(highlight.getDescription().replace("\u0000", ""));
        if (highlight.getLongitude() != null && highlight.getLatitude() != null) {
          try {
            poi.setLocation(new Point(Double.parseDouble(highlight.getLongitude()), Double.parseDouble(highlight.getLatitude())));
          } catch (NumberFormatException e) {
            Log.debug("Incorrect Lat/Long for Poi Id: "+ highlight.getId());
          } catch (Exception e) {
            Log.debug("Incorrect Lat/Long for Poi Id: "+ highlight.getId());
          }
        }
        poi.setData(poi.marshalData(highlight));
        poi.insert();
        insertedAfarPois++;
      }
      processedAfarPois++;

    }
    catch(Exception e) {
      Log.err("Error Processing AFAR POI ", e);
    }
  }


  @With({Authenticated.class, Authorized.class})
  public Result afarCityAutocomplete() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    final DynamicForm form = formFactory.form().bindFromRequest();
    String city = form.get("term");
    if (city != null) {
      city += "%";
    }

    //Map holds both Cities and Countries. And because a city and country can have same AfarId.
    //We will append "-CT" to signify a City e.g key = "24-CT"
    //and "-CY" for Country e.g key = "5-CY".
    Map<String, String> citiesAndCountries = getCitiesAndCountries(city);
    ObjectNode result = Json.newObject();
    ArrayNode array = result.arrayNode();
    if (citiesAndCountries != null) {
      for (String key: citiesAndCountries.keySet()) {
        ObjectNode classNode = Json.newObject();
        classNode.put("label", citiesAndCountries.get(key));
        classNode.put("id", key);
        array.add(classNode);
      }
    }

    return ok(array);

  }

  public static HashMap<String, String> getCitiesAndCountries(String name) {
    HashMap <String, String> citiesAndCountriesMap = new HashMap();
    if (name == null) {
      return null;
    }
    List<AfarCity> cities = AfarCity.findCitiesWithPoisByName(name);
    List<AfarCountry> countries = AfarCountry.findCountriesWithPoisByName(name);

    if (cities != null && cities.size() > 0) {
      for (AfarCity row: cities) {
        AfarCountry country = AfarCountry.find.byId(row.getAfarCountryId());
        String key = row.getAfarId() + "-CT";
        String value = row.getName();
        if (country != null) {
          value += ", "+country.getName();
        }
        citiesAndCountriesMap.put(key, value);
      }
    }

    if (countries != null && countries.size() > 0) {
      for (AfarCountry row: countries) {
        String key = row.getAfarId() + "-CY";
        String value = row.getName();
        citiesAndCountriesMap.put(key, value);
      }
    }

    return citiesAndCountriesMap;
  }


  @With({Authenticated.class, Authorized.class})
  public Result buildCityContent(String tripId, String afarId) {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    boolean isCountry = false;
    Integer locationId = -1;
    if (afarId != null && afarId.length() > 3) {
      int index = afarId.indexOf("-");
      if (index > 0) {
        if (afarId.substring(index).equalsIgnoreCase("-CY")) {
          isCountry = true;
        }
        locationId = Integer.valueOf(afarId.substring(0, index));
      }
    } else {
      Log.debug("BuildAfarView - Error with AfarId: "+ afarId);
    }
    AfarInfoView view = buildAfarView(tripId, locationId, isCountry, sessionMgr.getUserId() );
    if (view != null) {
      String content = views.html.trip.bookings.details.afarContent.render(view).toString();
      return ok(content);
    }
    return ok();
  }

  public static AfarInfoView buildAfarView(String tripId, int locationId, boolean isCountry, String userId) {
    AfarInfoView view = new AfarInfoView();

    if (tripId != null && locationId > -1) {
      view.tripId = tripId;
      Integer afarId = 0;
      if (isCountry) {
        AfarCountry country = AfarCountry.find.byId(locationId);
        if (country != null) {
          view.isCountry = true;
          view.countryId = country.getAfarId();
          view.cityName = country.getName();
          view.countryName = country.getName();
          afarId = country.getAfarId();
          view.overview = Utils.sanitizeRemoveAllHtml(country.getDescription());
          view.destId = country.getAfarDestId();
          view.heroImageUrl = country.getImageUrl();
          view.photoCredit = country.getPhotoCredit();
          view.url = "https://www.afar.com/travel-guides/" + country.getSlug() + "/guide";

          List<AfarCountryList> countryLists = AfarCountryList.findByCountryId(country.getAfarId());
          if (countryLists != null && countryLists.size() > 0) {
            view.afarLists = new ArrayList<>();
            for (AfarCountryList acl : countryLists) {
              AfarListView listView = buildCountryListView(acl, country);
              view.afarLists.add(listView);
            }
          }
        }
      } else {
        AfarCity city = AfarCity.find.byId(locationId);
        if (city != null) {
          AfarCountry country = AfarCountry.find.byId(city.getAfarCountryId());
          view.isCountry = false;
          view.cityId = city.getAfarId();
          afarId = city.getAfarId();

          view.cityName = city.getName();
          view.countryId = city.getAfarCountryId();
          if (country != null) {
            view.countryName = country.getName();
          }
          view.overview = Utils.sanitizeRemoveAllHtml(city.getDescription());
          view.destId = city.getAfarDestId();
          view.heroImageUrl = city.getImageUrl();
          view.photoCredit = city.getPhotoCredit();
          view.regionName = null;

          List<AfarCityList> cityLists = AfarCityList.findByCityId(city.getAfarId());
          if (cityLists != null && cityLists.size() > 0) {
            view.afarLists = new ArrayList<>();
            for (AfarCityList acl : cityLists) {
              AfarListView listView = buildCityListView(acl, city, country);
              view.afarLists.add(listView);

              if (view.regionName == null) {
                if (listView.regionName != null) {
                  view.regionName = listView.regionName;
                }
              }
            }
          }

          if (country.getAfarId() == 2) {
            if (view.regionName != null) {
              view.url = "https://www.afar.com/travel-guides/" + country.getSlug() + "/" + view.regionName.toLowerCase().replace(" ", "-") + "/" + city.getSlug() + "/guide";
            } else {
              view.url = "https://www.afar.com/travel-guides/" + country.getSlug() + "/guide";
            }
          } else {
            view.url = "https://www.afar.com/travel-guides/" + country.getSlug() + "/" + city.getSlug() + "/guide";
          }
        }
      }
      view.selectedAfarLists = selectListsToDisplay(view.afarLists, userId, afarId);

      if (view.selectedAfarLists != null && view.selectedAfarLists.size() > 2 && view.selectedAfarLists.get(2) != null) {
        view.selectedAfarLists.get(2).url = view.url + "?=&force_search=true";
      }

      return view;
    }
    return null;
  }

  public static AfarListView buildCityListView(AfarCityList acl, AfarCity city, AfarCountry country) {
    AfarListView listView = new AfarListView();
    listView.isCountry = false;
    listView.title = acl.getName();
    listView.listId = acl.getAfarId();
    listView.cityId = city.getAfarId();
    listView.cityName = city.getName();
    listView.countryId = city.getAfarCountryId();
    if (country != null) {
      listView.countryName = country.getName();
    }
    listView.destId = acl.getAfarDestId();
    listView.categoryType = acl.getType();
    listView.regionName = null;
    listView.url = "https://www.afar.com/travel-tips/" + acl.getSlug();

    AfarList listData = acl.unmarshalData(acl.getData());
    if (listData != null && listData.getHighlights() != null && listData.getHighlights().size() > 0) {
      listView.afarPois = new ArrayList<>();
      for (Highlight poi : listData.getHighlights()) {
        AfarPoiView poiView = buildPoiView(poi, city, country);
        listView.afarPois.add(poiView);

        if (country.getAfarId() == 2) {
          if (listView.regionName == null && poi.getRegion() != null) {
            listView.regionName = poi.getRegion();
          }
        }
      }
    }

    return listView;
  }

  public static AfarListView buildCountryListView(AfarCountryList acl, AfarCountry country) {
    AfarListView listView = new AfarListView();
    listView.isCountry = true;
    listView.title = acl.getName();
    listView.listId = acl.getAfarId();
    listView.cityName = country.getName();
    listView.countryId = country.getAfarId();
    listView.countryName = country.getName();
    listView.destId = acl.getAfarDestId();
    listView.categoryType = acl.getType();
    listView.url = "https://www.afar.com/travel-tips/" + acl.getSlug();

    AfarList listData = acl.unmarshalData(acl.getData());
    if (listData != null && listData.getHighlights() != null && listData.getHighlights().size() > 0) {
      listView.afarPois = new ArrayList<>();
      for (Highlight poi : listData.getHighlights()) {
        AfarPoiView poiView = buildPoiView(poi, null, country);
        listView.afarPois.add(poiView);
      }
    }
    return listView;
  }

  public static AfarPoiView buildPoiView(Highlight poi, AfarCity city, AfarCountry country) {
    AfarPoiView poiView = new AfarPoiView();
    poiView.poiId = poi.getId();
    poiView.title = poi.getTitle();
    poiView.placeName = poi.getPlaceName();

    if (city == null && country != null) {
      poiView.cityName = country.getName();
    } else if (city != null){
      poiView.cityId = city.getAfarId();
      poiView.cityName = city.getName();
    }

    if (country != null) {
      poiView.countryId = country.getAfarId();
      poiView.countryName = country.getName();
    }

    if (city == null && country == null) {
      if (poi.getCityId() != null) {
        poiView.cityId = poi.getCityId();
      }
      if (poi.getCity() != null) {
        poiView.cityName = poi.getCity();
      }
      if (poi.getCountryId() != null) {
        poiView.countryId = poi.getCountryId();
      }
      if (poi.getCountry() != null) {
        poiView.countryName = poi.getCountry();
      }
    }

    if (poi.getCountryId() != null && poi.getCountryId() == 2) {
      if (poi.getCountry() != null && poi.getRegion() != null && poi.getCity() != null) {
        poiView.url = "https://www.afar.com/travel-guides/" + poi.getCountry().toLowerCase().replace(" ", "-") + "/" + poi.getRegion().toLowerCase().replace(" ", "-") + "/" + poi.getCity().toLowerCase().replace(" ", "-") + "/guide";
      } else if (poi.getCountry() != null){
        poiView.url = "https://www.afar.com/travel-guides/" + poi.getCountry().toLowerCase().replace(" ", "-") + "/guide";
      }
    } else if (poi.getCountry() != null && poi.getCity() != null){
      poiView.url = "https://www.afar.com/travel-guides/" + poi.getCountry().toLowerCase().replace(" ", "-") + "/" + poi.getCity().toLowerCase().replace(" ", "-") + "/guide";

    }

    poiView.address = poi.getAddress();
    poiView.locationPhone = poi.getLocationPhoneNumber();
    poiView.latitude = Double.parseDouble(poi.getLatitude());
    poiView.longitude = Double.parseDouble(poi.getLongitude());
    poiView.description = Utils.sanitizeRemoveAllHtml(poi.getDescription());
    poiView.categories = poi.getCategories();
    if (poi.getImages() != null && poi.getImages().size() > 0) {
      poiView.images = new ArrayList<>();
      for (AfarImage image : poi.getImages()) {
        ImageView imageView = new ImageView();
        imageView.url = image.getLargeImageUrl();
        imageView.thumbUrl = image.getSmallImageUrl();
        imageView.imgSrc = image.getAttribution();
        poiView.images.add(imageView);
      }
    }
    return poiView;
  }

  public static Map<Integer, AfarListView> selectListsToDisplay(List<AfarListView> afarLists, String userId, Integer afarId) {
    String cacheKey = null;
    if (userId != null && afarId > 0 && afarLists != null && afarLists.size() > 0 && !StringUtils.isNumber(userId)) {
      //since the selection of pois for a city are randomized, we cache the selection for the pub user
      //so that what is displayed in the publisher is the same as what is added to the trip

      cacheKey = APPConstants.CACHE_AFAR_LIST_SELECTIONS + userId + afarId + afarLists.size();
    }

    Map<Integer, AfarListView> selectedAfarLists = null;
    List<AfarPoiView> selectedPoiViews = new ArrayList<>();
    if (afarLists != null) {
      selectedAfarLists = new HashMap<>();
      if (afarLists.size() == 1) {
        AfarListView v = afarLists.get(0);
        if (v.categoryType != null && !v.categoryType.toLowerCase().trim().equals("stay")) {
          selectedAfarLists.put(0, afarLists.get(0));
        }

      } else if (afarLists.size() > 1) {
        //remove stays
        Iterator<AfarListView> it = afarLists.iterator();
        while (it.hasNext()) {
          AfarListView v = it.next();
          if (v.categoryType != null && v.categoryType.toLowerCase().trim().equals("stay")) {
            it.remove();
          }
        }
        ArrayList<Integer> positions = null;

        if (cacheKey != null) {
          positions = (ArrayList<Integer>) CacheMgr.get(cacheKey);
        }
        if (positions == null) {
          positions = new ArrayList<>();
          if (afarLists.size() > 3) {
            for (int i = 0; i < 20; i++) {
              int randomNum = ThreadLocalRandom.current().nextInt(0, afarLists.size());
              if (!positions.contains(randomNum)) {
                positions.add(randomNum);
              }
            }
          } else {
            if (afarLists.size() > 0) {
              positions.add(0);
            }
            if (afarLists.size() > 1) {
              positions.add(1);
            }
            if (afarLists.size() > 2) {
              positions.add(2);
            }
          }
          //save this to the cach for this user + destination
          if (cacheKey != null && positions.size() > 2) {
            CacheMgr.set(cacheKey, positions, APPConstants.CACHE_TRANSIENT_EXPIRY_SECS);
          }
        }

        int count = 0;
        int index = 0;

        for (Integer i : positions) {
          if (count == 3) {
            break;
          }
          count++;
          if (positions.size() == 1) {
            if (afarLists.get(0).afarPois.size() > 3) {
              AfarListView listView2 = new AfarListView();
              int endIndex = afarLists.get(0).afarPois.size();
              List<AfarPoiView> subPoiList = afarLists.get(0).afarPois.subList(3, endIndex);
              listView2.afarPois = selectPoisToDisplay(subPoiList, selectedPoiViews);
              if (listView2.afarPois.size() > 0) {
                selectedAfarLists.put(2, listView2);
                selectedPoiViews.addAll(listView2.afarPois);
              }
            }

            AfarListView listView = afarLists.get(0);
            listView.afarPois = selectPoisToDisplay(listView.afarPois, selectedPoiViews);
            if (listView.afarPois.size() > 0) {
              selectedAfarLists.put(0, listView);
              selectedPoiViews.addAll(listView.afarPois);
            }
          } else {


            if (afarLists.get(i) != null) {
              AfarListView listView = afarLists.get(i);
              listView.afarPois = selectPoisToDisplay(listView.afarPois, selectedPoiViews);
              if (listView.afarPois.size() > 0) {
                selectedAfarLists.put(index, listView);
                selectedPoiViews.addAll(listView.afarPois);
                index++;
              }
            }


          }

        }


      }
    }

    return selectedAfarLists;
  }

  public static List<AfarPoiView> selectPoisToDisplay(List<AfarPoiView> afarPois, List<AfarPoiView> selectedPoiViews) {
    List<AfarPoiView> selectedAfarPois = new ArrayList<>();
    if (afarPois != null && afarPois.size() > 1) {
      for (int i = 0; i < afarPois.size() && selectedAfarPois.size() <= 3 ; i++ ) {
        AfarPoiView poiView = afarPois.get(i);
        if (poiView != null && !selectedPoiViews.contains(poiView) && !poiView.categories.contains("Stay") && poiView.description != null && poiView.description.trim().length() > 0) {
          selectedAfarPois.add(afarPois.get(i));
        }
      }
    }
    return selectedAfarPois;
  }

  @With({Authenticated.class, Authorized.class})
  public Result addAfarSelectedContent() {
    StopWatch sw = new StopWatch();
    sw.start();

    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    final DynamicForm form = formFactory.form().bindFromRequest();
    String inAfarId = form.get("inAfarId");
    String tripId = form.get("afarTripId");
    String inNoteDate = form.get("inAfarNoteDate");

    boolean isCountry = false;
    Integer locationId = -1;
    if (inAfarId != null && inAfarId.length() > 3) {
      int index = inAfarId.indexOf("-");
      if (index > 0 && inAfarId.substring(index).equalsIgnoreCase("-CY")) {
        isCountry = true;
      }
      if (index > 0) {
        try {
          locationId = Integer.valueOf(inAfarId.substring(0, index));
        } catch (Exception e) {
          Log.err("addAfarSelectedContent - Invalid afarid: tripid: " + tripId + " locationid:" + locationId + " index:" +index);
        }
      }
    }

    if (tripId == null || inAfarId == null) {
      BaseView baseView = new BaseView();
      baseView.message = "Errors on the form - please resubmit";
      return ok(views.html.common.message.render(baseView));
    }

    Trip tripModel = Trip.findByPK(tripId);

    if (tripModel != null && SecurityMgr.getTripAccessLevel(tripModel, sessionMgr).ge(SecurityMgr.AccessLevel.APPEND)) {

      try {
        int response = insertAfarGuideAsTripNote(tripModel, locationId, inNoteDate, sessionMgr.getUserId(), isCountry);

        if (response == APPConstants.INSERT_AFAR_NOTE_SUCCESS) {
          BookingController.invalidateCache(tripId, null);
          flash(SessionConstants.SESSION_PARAM_MSG, "AFAR guide added successfully.");
          return redirect(routes.BookingController.bookings(tripModel.tripid, new TabType.Bound(TabType.AFAR), null));
        } else if (response == APPConstants.UPDATE_AFAR_NOTE_SUCCESS) {
          BookingController.invalidateCache(tripId, null);
          flash(SessionConstants.SESSION_PARAM_MSG, "AFAR guide already exists. Updated successfully.");
          return redirect(routes.BookingController.bookings(tripModel.tripid, new TabType.Bound(TabType.AFAR), null));
        } else {
          BaseView baseView = new BaseView();
          baseView.message = "System Error - AFAR guide was not added";
          return ok(views.html.common.message.render(baseView));
        }

      } catch (Exception e) {
        Ebean.rollbackTransaction();
        Log.log(LogLevel.DEBUG, "Error adding AFAR guide", e);
        BaseView baseView = new BaseView();
        baseView.message = "System Error - please resubmit";
        return ok(views.html.common.message.render(baseView));
      }
    } else {
      BaseView baseView = new BaseView();
      baseView.message = "Cannot access this guide  - please resubmit";
      return ok(views.html.common.message.render(baseView));
    }

  }


  public static Integer autoAddAllAfarGuides(Trip trip, Account account, OfferService offerService) {
    if (trip == null) {
      Log.log(LogLevel.ERROR, "autoAddAllAfarGuides: Trip does not exist");
      return APPConstants.INSERT_AFAR_NOTE_ERROR;
    }

    //First Delete ALL AFAR Guides
    int deleteResponse = deleteAllAfarGuidesFromTrip(trip.tripid);


    ItineraryOffer offer = offerService.getOffer(trip.tripid);
    if (offer != null && offer.getLocationOffers().size() > 0) {
      try {

        int inserted = 0;
        int failed = 0;

        for (LocationOffer locationOffer : offer.getLocationOffers()) {
          UMappedCity umCity = locationOffer.getLocation();
          if (umCity != null) {
            AfarCity afarCity = AfarCity.findByUmCity(umCity);
            if (afarCity != null) {
              List<AfarCityList> cityLists = AfarCityList.findByCityId(afarCity.getAfarId());
              if (cityLists != null && cityLists.size() > 0) {
                int response = AfarController.insertAfarGuideAsTripNote(trip, afarCity.getAfarId(), null, account.getLegacyId(), false);

                if (response == APPConstants.INSERT_AFAR_NOTE_SUCCESS) {
                  Log.debug("Successfully added AFAR guide to Trip: " + trip.tripid + " for UmCity: " + umCity.getId() + " with AfarId: " + afarCity.getAfarId() + " - " + afarCity.getName());
                  inserted++;

                } else {
                  Log.err("Error adding AFAR guide to Trip: " + trip.tripid + " for UmCity: " + umCity.getId() + " with AfarId: " + afarCity.getAfarId() + " - " + afarCity.getName());
                  failed++;
                }
              } else {
                Log.err("Data unavailable - AfarCityList does not exist for AfarCity: " + afarCity.getAfarId());
                failed++;
              }
            } else {
              Log.err("Data unavailable - AfarCity does not exist for UmCity: " + umCity.getId());
              failed++;
            }
          } else {
            Log.err("Data unavailable - UmCity does not exist for Location Offer: " + locationOffer.toString());
            failed++;
          }
        }

        Log.debug(inserted + " AFAR guide(s) auto added to Itinerary. " + failed + " AFAR guide(s) not added.");

        BookingController.invalidateCache(trip.tripid, null);
        return APPConstants.INSERT_AFAR_NOTE_SUCCESS;

      } catch (Exception e) {
        Ebean.rollbackTransaction();
        Log.log(LogLevel.ERROR, "Error auto processing AFAR guides into Itinerary ", e);
        return APPConstants.INSERT_AFAR_NOTE_ERROR;
      }
    } else {
      Log.debug("No available AFAR guides to update or add to this Itinerary");
      return APPConstants.INSERT_AFAR_NOTE_SUCCESS;

    }

  }


  /*
  * This helper method processes and adds the AFAR guide as a note to the itinerary.
  * It checks for duplicates and ignores duplicates.
  *
  * Returns:
  * -1 = Error
  * 0 = Duplicates
  * 1 = Successful
  */
  public static int insertAfarGuideAsTripNote (Trip tripModel, Integer inAfarId, String inNoteDate, String userId, Boolean isCountry) throws Exception {
    if (tripModel != null && inAfarId != null) {
      AfarInfoView view = buildAfarView(tripModel.tripid, inAfarId, isCountry, userId);

      String content = "";
      String noteTitle = "AFAR Destination Guide";
      if (view != null) {
        content = views.html.trip.bookings.details.afarContent.render(view).toString();
        if (view.isCountry) {
          noteTitle = "AFAR Country Guide";
          if (view.cityName != null && !view.cityName.isEmpty()) {
            noteTitle += " - " + view.cityName;
          }
        } else {
          noteTitle = "AFAR City Guide";
          if (view.cityName != null && !view.cityName.isEmpty()) {
            noteTitle += " - " + view.cityName;
          }
          if (view.countryName != null && !view.countryName.isEmpty()) {
            noteTitle += ", " + view.countryName;
          }
        }
      } else {
        return APPConstants.INSERT_AFAR_NOTE_ERROR;
      }

      TripNote existingNote = TripNote.findActiveUniqueAfarGuideInTrip(tripModel.tripid, noteTitle);
      if (existingNote == null) {
        Ebean.beginTransaction();
        TripNote tn = TripNote.buildTripNote(tripModel, userId);
        StringBuilder sb = new StringBuilder();
        tn.setName(noteTitle);
        if (inNoteDate != null) {
          try {
            long ms = Utils.getMilliSecs(inNoteDate);
            if (ms > 1) {
              tn.setNoteTimestamp(ms);
            }
          } catch (Exception e) {
            Log.log(LogLevel.ERROR, "AfarController: Error setting NoteTimestamp: "+ inNoteDate, e);
            tn.setTag(APPConstants.NOTE_TAG_LAST_ITEM);
          }
        } else {
          tn.setTag(APPConstants.NOTE_TAG_LAST_ITEM);
        }

        if (tn.getTag() == null || tn.getTag().isEmpty()) {
          tn.setTag(APPConstants.AFAR_NOTE_TAG);
        } else {
          if (!tn.getTag().contains(APPConstants.AFAR_NOTE_TAG)) {
            tn.setTag(tn.getTag() + ", " + APPConstants.AFAR_NOTE_TAG);
          }
        }
        tn.setIntro(content);
        int currRows = TripNote.tripNoteCount(tripModel.tripid);
        tn.setRank(++currRows);
        tn.setType(TripNote.NoteType.CONTENT);
        tn.save();
        Ebean.commitTransaction();

        return APPConstants.INSERT_AFAR_NOTE_SUCCESS;

      } else {
        Ebean.beginTransaction();
        existingNote.setIntro(content);
        if (inNoteDate != null) {
          try {
            long ms = Utils.getMilliSecs(inNoteDate);
            if (ms > 1) {
              existingNote.setNoteTimestamp(ms);
            }
          } catch (Exception e) {
            Log.log(LogLevel.ERROR, "AfarController: Error setting NoteTimestamp: "+ inNoteDate, e);
            if (existingNote.getNoteTimestamp() == null || existingNote.getNoteTimestamp()  < 1L) {
              if (existingNote.getTag() == null || existingNote.getTag().isEmpty()) {
                existingNote.setTag(APPConstants.NOTE_TAG_LAST_ITEM);
              } else if (!existingNote.getTag().contains(APPConstants.NOTE_TAG_LAST_ITEM)) {
                existingNote.setTag(APPConstants.NOTE_TAG_LAST_ITEM + ", " + existingNote.getTag());
              }
            }
          }
        } else {
          if (existingNote.getNoteTimestamp() == null || existingNote.getNoteTimestamp()  < 1L) {
            if (existingNote.getTag() == null || existingNote.getTag().isEmpty()) {
              existingNote.setTag(APPConstants.NOTE_TAG_LAST_ITEM);
            } else if (!existingNote.getTag().contains(APPConstants.NOTE_TAG_LAST_ITEM)) {
              existingNote.setTag(APPConstants.NOTE_TAG_LAST_ITEM + ", " + existingNote.getTag());
            }
          }

        }

        if (existingNote.getTag() == null || existingNote.getTag().isEmpty()) {
          existingNote.setTag(APPConstants.AFAR_NOTE_TAG);
        } else {
          if (!existingNote.getTag().contains(APPConstants.AFAR_NOTE_TAG)) {
            existingNote.setTag(existingNote.getTag() + ", " + APPConstants.AFAR_NOTE_TAG);
          }
        }
        existingNote.update();
        Ebean.commitTransaction();
        return APPConstants.UPDATE_AFAR_NOTE_SUCCESS;
      }
    }
    return APPConstants.INSERT_AFAR_NOTE_ERROR;
  }

  /*
  * This helper method deletes all AFAR guides from the itinerary.
  * Mostly called as apart of the AutoPublish/AutoAfarGuides process..
  *
  * Returns:
  * -1 = Error
  * 1 = Successful
  */
  public static int deleteAllAfarGuidesFromTrip (String tripId) {
    if (tripId != null) {
      try {
        List<TripNote> existingNotes = TripNote.findAllActiveAfarGuidesInTrip(tripId);
        if (existingNotes != null && existingNotes.size() > 0) {
          Ebean.beginTransaction();
          for (TripNote tn : existingNotes) {
            tn.delete();
          }
          Ebean.commitTransaction();
        }
        return APPConstants.INSERT_AFAR_NOTE_SUCCESS;
      } catch (Exception e) {
        Ebean.rollbackTransaction();
        Log.log(LogLevel.DEBUG, "Error while deleting all AFAR Guides ", e);
      }
    }
    return APPConstants.INSERT_AFAR_NOTE_ERROR;
  }

  @With({Authenticated.class, Authorized.class})
  public Result addAfarSelectedPoi() {
    StopWatch sw = new StopWatch();
    sw.start();

    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    final DynamicForm form = formFactory.form().bindFromRequest();
    String inAfarId = form.get("inPoiAfarId");
    String tripId = form.get("poiTripId");
    String inNoteDate = form.get("inPoiNoteDate");

    if (tripId == null || inAfarId == null) {
      BaseView baseView = new BaseView();
      baseView.message = "Errors on the form - please resubmit";
      return ok(views.html.common.message.render(baseView));
    }

    Integer afarId = Integer.parseInt(inAfarId);

    Trip tripModel = Trip.findByPK(tripId);
    AfarPoi poi = AfarPoi.find.byId(afarId);

    if (tripModel != null && poi != null && SecurityMgr.getTripAccessLevel(tripModel, sessionMgr).ge(SecurityMgr.AccessLevel.APPEND)) {
      Highlight poiData = poi.unmarshalData(poi.getData());
      AfarPoiView view = buildPoiView(poiData, null, null);
      String content = "";
      String noteTitle = "AFAR Highlight";
      if (view != null) {
        content = views.html.trip.bookings.details.afarPoi.render(view).toString();
        if (view.title != null && !view.title.isEmpty()) {
          noteTitle += " - " + view.title;
        }
        String remNoteTitle = "  (";
        if (view.cityName != null && !view.cityName.isEmpty()) {
            remNoteTitle += view.cityName;
        }
        if (view.countryName != null && !view.countryName.isEmpty()) {
          if (remNoteTitle.length() > 4) {
            remNoteTitle += ", ";
          }
          remNoteTitle += view.countryName;
        }
        remNoteTitle += ")";
        if (remNoteTitle.length() > 7) {
          noteTitle += remNoteTitle;
        }
      }


      try {
        Ebean.beginTransaction();

        TripNote tn = TripNote.buildTripNote(tripModel, sessionMgr.getUserId());

        tn.setName(noteTitle);

        if (inNoteDate != null) {
          try {
            long ms = Utils.getMilliSecs(inNoteDate);
            if (ms > 1) {
              tn.setNoteTimestamp(ms);
            }
          } catch (Exception e) {

          }
        } else {
          tn.setTag(APPConstants.NOTE_TAG_LAST_ITEM);
        }

        if (tn.getTag() == null || tn.getTag().isEmpty()) {
          tn.setTag(APPConstants.AFAR_NOTE_TAG);
        } else {
          if (!tn.getTag().contains(APPConstants.AFAR_NOTE_TAG)) {
            tn.setTag(tn.getTag() + ", " + APPConstants.AFAR_NOTE_TAG);
          }
        }

        tn.setIntro(content);
        int currRows = TripNote.tripNoteCount(tripModel.tripid);
        tn.setRank(++currRows);
        tn.setType(TripNote.NoteType.CONTENT);
        tn.save();
        Ebean.commitTransaction();


        BookingController.invalidateCache(tripId, null);
        flash(SessionConstants.SESSION_PARAM_MSG, "AFAR Highlight added successfully");
        return redirect(routes.BookingController.bookings(tripModel.tripid, new TabType.Bound(TabType.AFAR), null));

      } catch (Exception e) {
        Ebean.rollbackTransaction();
        Log.log(LogLevel.DEBUG, "Error adding AFAR Highlight ", e);
        BaseView baseView = new BaseView();
        baseView.message = "System Error - please resubmit";
        return ok(views.html.common.message.render(baseView));
      }
    } else {
      BaseView baseView = new BaseView();
      baseView.message = "Cannot access this Highlight  - please resubmit";
      return ok(views.html.common.message.render(baseView));
    }

  }

  @With({Authenticated.class, Authorized.class})
  public Result addAfarSelectedPois() {
    StopWatch sw = new StopWatch();
    sw.start();

    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    final DynamicForm form = formFactory.form().bindFromRequest();
    String inAfarIds = form.get("inPoiAfarIds");
    String tripId = form.get("poiTripId");
    String inNoteDate = form.get("inPoiNoteDate");

    if (tripId == null || inAfarIds == null) {
      BaseView baseView = new BaseView();
      baseView.message = "Errors on the form - please resubmit";
      return ok(views.html.common.message.render(baseView));
    }

    String[] arrAfarIds = null;
    if (inAfarIds != null && !inAfarIds.isEmpty()) {
      arrAfarIds = inAfarIds.split(",");
    }

    Trip tripModel = Trip.findByPK(tripId);

    if (tripModel != null && SecurityMgr.getTripAccessLevel(tripModel, sessionMgr).ge(SecurityMgr.AccessLevel.APPEND)) {

      if (arrAfarIds != null && arrAfarIds.length > 0) {
        Ebean.beginTransaction();
        for (String id : arrAfarIds) {

          Integer afarId = Integer.parseInt(id.trim());

          AfarPoi poi = AfarPoi.find.byId(afarId);

          if (poi != null) {
            Highlight poiData = poi.unmarshalData(poi.getData());
            AfarPoiView view = buildPoiView(poiData, null, null);
            String content = "";
            String noteTitle = "AFAR Highlight";
            if (view != null) {
              content = views.html.trip.bookings.details.afarPoi.render(view).toString();
              if (view.title != null && !view.title.isEmpty()) {
                noteTitle += " - " + view.title;
              }
              String remNoteTitle = "  (";
              if (view.cityName != null && !view.cityName.isEmpty()) {
                remNoteTitle += view.cityName;
              }
              if (view.countryName != null && !view.countryName.isEmpty()) {
                if (remNoteTitle.length() > 4) {
                  remNoteTitle += ", ";
                }
                remNoteTitle += view.countryName;
              }
              remNoteTitle += ")";
              if (remNoteTitle.length() > 7) {
                noteTitle += remNoteTitle;
              }
            }


            try {
              TripNote tn = TripNote.buildTripNote(tripModel, sessionMgr.getUserId());

              tn.setName(noteTitle);

              if (inNoteDate != null) {
                try {
                  long ms = Utils.getMilliSecs(inNoteDate);
                  if (ms > 1) {
                    tn.setNoteTimestamp(ms);
                  }
                } catch (Exception e) {

                }
              } else {
                tn.setTag(APPConstants.NOTE_TAG_LAST_ITEM);
              }

              if (tn.getTag() == null || tn.getTag().isEmpty()) {
                tn.setTag(APPConstants.AFAR_NOTE_TAG);
              } else {
                if (!tn.getTag().contains(APPConstants.AFAR_NOTE_TAG)) {
                  tn.setTag(tn.getTag() + ", " + APPConstants.AFAR_NOTE_TAG);
                }
              }

              tn.setIntro(content);
              int currRows = TripNote.tripNoteCount(tripModel.tripid);
              tn.setRank(++currRows);
              tn.setType(TripNote.NoteType.CONTENT);
              tn.save();

            } catch (Exception e) {
              Ebean.rollbackTransaction();
              Log.log(LogLevel.DEBUG, "Error adding AFAR Highlight ", e);
              BaseView baseView = new BaseView();
              baseView.message = "System Error - please resubmit";
              return ok(views.html.common.message.render(baseView));
            }
          }
        }
        Ebean.commitTransaction();

        BookingController.invalidateCache(tripId, null);
        flash(SessionConstants.SESSION_PARAM_MSG, "AFAR Highlight added successfully");
        return redirect(routes.BookingController.bookings(tripModel.tripid, new TabType.Bound(TabType.AFAR), null));
      }
    } else {
      BaseView baseView = new BaseView();
      baseView.message = "Error - Cannot access Trip";
      return ok(views.html.common.message.render(baseView));
    }
    BaseView baseView = new BaseView();
    baseView.message = "Error - please resubmit";
    return ok(views.html.common.message.render(baseView));

  }


  @With({Authenticated.class, Authorized.class})
  public Result getFilteredAfarPois(String tripId, String locationId, Integer startPos, String keyword, String type) {
    Log.debug("LocationId: " + locationId);
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    final DynamicForm form = formFactory.form().bindFromRequest();
    String city = form.get("term");
    if (keyword == null || keyword.isEmpty()) {
      keyword = "%";
    } else {
      keyword = "%"+keyword.replace(" ", "%")+"%";
    }

    if (type != null && (type.equals("Stay") || type.equals("Eat") || type.equals("Do") || type.equals("Drink") || type.equals("Shop"))){
      type = "%\"" + type.toUpperCase() + "\"%";
    } else {
      type = "%";
    }

    boolean isCountry = false;
    Integer afarId = -1;
    if (afarId != null) {
      int index = locationId.indexOf("-");
      if (index > 0 && locationId.substring(index).equalsIgnoreCase("-CY")) {
        isCountry = true;
      }
      try {
        afarId = Integer.valueOf(locationId.substring(0, index));
      } catch (Exception e) {
        Log.err("AfarController:getFilteredAfarPois - Invalid afarid: tripid: " + tripId + " locationid:" + locationId + " startPos:" +startPos + " keyword:" +keyword + " type:" +type + " index:" +index);
      }
    }
    List<AfarPoi> pois = AfarPoi.findPoisByKeywordDestination(afarId, keyword, isCountry, startPos, MAX_AFAR_POIS, type);
    int totalPois = AfarPoi.countPoisByKeywordDestination(afarId, keyword, isCountry, type);

    List<AfarPoiView> poiViews = new ArrayList<>();
    List<Highlight> highlights = null;
    AfarPoiResults results = null;
    if (pois != null && pois.size() > 0) {
      highlights = new ArrayList<>();
      for (AfarPoi row: pois) {
        Highlight h = row.unmarshalData(row.getData());
        highlights.add(h);
      }
    }
    if (highlights != null) {
      results = new AfarPoiResults(highlights);
      if (highlights.size() == MAX_AFAR_POIS) {
        //there is more so let's figure out the next start
        results.setNextPos(startPos + MAX_AFAR_POIS);
      }
      if (startPos >= MAX_AFAR_POIS) {
        results.setPrevPos(startPos - MAX_AFAR_POIS);
      }
      if (keyword != null && !keyword.isEmpty()) {
        results.setKeyword(keyword);
      }
      results.setFromIndex(startPos + 1);
      results.setToIndex(startPos + highlights.size());
      results.setTotalPois(totalPois);
    }

    if (results != null) {
      return ok(objectMapper.convertValue(results, JsonNode.class));
    }

    return ok();

  }

  @With({Authenticated.class, Authorized.class})
  public Result getAvailableCities(String tripId) {
    Map<String, String> citiesMap = new HashMap<>();
    try {
      ItineraryOffer offer = offerService.getOffer(tripId);
      if (offer != null && offer.getLocationOffers().size() > 0) {
        CacheMgr.set(APPConstants.CACHE_TRIP_OFFER + tripId, offer);
        for (LocationOffer locationOffer : offer.getLocationOffers()) {
          UMappedCity umCity = locationOffer.getLocation();
          if (umCity != null) {
            AfarCity afarCity = AfarCity.findByUmCity(umCity);
            if (afarCity != null) {
              List<AfarCityList> cityLists = AfarCityList.findByCityId(afarCity.getAfarId());
              if (cityLists != null && cityLists.size() > 0) {
                AfarCountry country = AfarCountry.find.byId(afarCity.getAfarCountryId());
                String key = afarCity.getAfarId() + "-CT";
                String value = afarCity.getName();
                if (country != null) {
                  value += ", "+country.getName();
                }
                citiesMap.put(key, value);
              }
            }
          }
        }
        if (citiesMap.size() > 0) {
          return ok(objectMapper.convertValue(citiesMap, JsonNode.class));
        }
      }
    } catch (Exception e) {
      Log.log(LogLevel.ERROR, "AfarController: Error while finding available Cities for Itinerary", e);
    }

    return ok();
  }

  //Helper method to check if Trip has any available Afar Guides that could be added (including the already added guides)
  public static Integer afarGuidesAvailableForTrip(String tripid, OfferService service) {
    int result = 0;
    try {
      ItineraryOffer offer = service.getOffer(tripid);
      if (offer != null && offer.getLocationOffers().size() > 0) {
        for (LocationOffer locationOffer : offer.getLocationOffers()) {
          UMappedCity umCity = locationOffer.getLocation();
          if (umCity != null) {
            AfarCity afarCity = AfarCity.findByUmCity(umCity);
            if (afarCity != null) {
              List<AfarCityList> cityLists = AfarCityList.findByCityId(afarCity.getAfarId());
              if (cityLists != null && cityLists.size() > 0) {
                result++;
              }
            }
          }
        }
      }
    } catch (Exception e) {
      Log.log(LogLevel.ERROR, "AfarController: Error while finding available Guides", e);
    }

    return result;
  }


  public static HashMap<Integer, String> getPoisByKeywordDestination(String afarId, String keyword) {
    HashMap <Integer, String> poisMap = new HashMap();
    if (afarId == null && afarId.length() < 1) {
      return null;
    }
    boolean isCountry = false;
    Integer locationId = -1;
    if (afarId != null) {
      int index = afarId.indexOf("-");
      if (index > 0 && afarId.substring(index).equalsIgnoreCase("-CY")) {
        isCountry = true;
      }
      locationId = Integer.valueOf(afarId.substring(0, index));
    }
    List<AfarPoi> pois = AfarPoi.findPoisByKeywordDestination(locationId, keyword, isCountry, 0, null, "%");

    if (pois != null && pois.size() > 0) {
      for (AfarPoi row: pois) {
        Integer key = row.getAfarId();
        String value = row.getTitle();
        poisMap.put(key, value);
      }
    }

    return poisMap;
  }
  

}
