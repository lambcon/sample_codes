package actors;

import akka.actor.*;
import com.google.inject.Inject;
import com.mapped.publisher.common.ConfigMgr;
import com.mapped.publisher.persistence.communicator.Communicator;
import com.mapped.publisher.utils.Log;
import com.umapped.helper.TripPublisherHelper;
import models.publisher.FlightAlert;
import models.publisher.FlightAlertEvent;
import scala.concurrent.duration.Duration;

import java.io.Serializable;
import java.util.concurrent.TimeUnit;

/**
 * Created by surge on 2015-09-01.
 */
public class FirebaseActor
    extends UntypedActor {

  public interface Factory {
    Actor create();
  }

  public final static Props props = Props.create(FirebaseActor.class);

  public FirebaseActor() {
    msgCounter = 0;
    int pingCycleSec = 60;
    try {
      pingCycleSec = Integer.parseInt(ConfigMgr.getAppParameter(com.mapped.common.CoreConstants.FIREBASE_PING_SEC));
    } catch (Exception e) {
      Log.err("FirebaseActor: misconfigured ENV variable FIREBASE_PING_SEC. Must be integer seconds value");
    }

    PING_CYCLE_SEC = pingCycleSec;
    PING_DELAY_MS = PING_CYCLE_SEC * (Communicator.FB_EVICTION_CYCLES - 1) * 1000;
  }

  public final int PING_CYCLE_SEC;// =  10; //Seconds
  /**
   * Delaying update tick start so that we have time to get information about all other nodes
   */
  private final int PING_DELAY_MS;

  private final static int LEADER_DELAY_MS = 0;   //1.1 seconds delay
  private final static int LEADER_TICK_SEC = 1800; //Seconds (set to 30 minutes => 1800sec)

  Communicator fc;
  private Cancellable clusterStatusTick;
  private Cancellable clusterLeaderTick;

  public static int msgCounter;

  @Inject
  TripPublisherHelper tripPublisher;

  public static class CommunicatorCommand
      implements Serializable {
    final Type type;
    final String testMsg;
    /**
     * In case when message is of NOTIFICATION_FLIGHT type, this has to be set
     */
    final FlightAlert flightAlert;
    final FlightAlertEvent flightAlertEvent;

    public enum Type
        implements Serializable {

      /**
       * Initiate periodic ping requests
       */
      PING_START,
      /**
       * Update server status timestamp
       */
      PING,

      /**
       * Stopping PING
       */
      PING_STOP,

      /**
       * Initiate periodic LEADER requests
       */
      LEADER_START,

      /**
       * Perform LEADER duties
       */
      LEADER,

      /**
       * Stop LEADER requests
       */
      LEADER_STOP,

      /**
       * Send out flight notifications.
       * Must set FlightAlert parameter when this message is sent
       */
      NOTIFICATION_FLIGHT,
      /**
       * Command that will be sent only in Chaos Mode to connect to the Firebase
       */
      CHAOS_CONNECT,
      /**
       * Command that will be sent only in Chaos Mode to disconnect from the Firebase
       */
      CHAOS_DISCONNECT,
      AKKA_TEST_1,
      AKKA_TEST_2
    }

    public CommunicatorCommand(Type type) {
      this(type, null);
    }

    public CommunicatorCommand(Type type, String msg) {
      this.type = type;
      testMsg = msg;
      flightAlert = null;
      flightAlertEvent = null;
    }

    public CommunicatorCommand(FlightAlert alert, FlightAlertEvent event) {
      this.type = Type.NOTIFICATION_FLIGHT;
      flightAlert = alert;
      flightAlertEvent = event;
      testMsg = null;
    }

    public FlightAlertEvent getFlightAlertEvent() {
      return flightAlertEvent;
    }

    public FlightAlert getFlightAlert() {
      return flightAlert;
    }
  }

  @Override
  public ActorContext context() {
    return super.context();
  }

  /**
   * User overridable callback.
   * <p/>
   * Is called asynchronously after 'actor.stop()' is invoked.
   * Empty default implementation.
   */
  @Override public void postStop()
      throws Exception {

    Log.err("Actor: " + this.hashCode() + " POST STOP: "+ self().path());
    super.postStop();
  }

  /**
   * User overridable callback.
   * <p/>
   * Is called when an Actor is started.
   * Actor are automatically started asynchronously when created.
   * Empty default implementation.
   */
  @Override public void preStart()
      throws Exception {

    Log.err("Actor: " + this.hashCode() + " PRE START: " + self().path());
    super.preStart();
  }

  /**
   * User overridable callback: By default it calls `preStart()`.
   * <p/>
   * Is called right AFTER restart on the newly created Actor to allow reinitialization after an Actor crash.
   *
   * @param reason
   */
  @Override public void postRestart(Throwable reason)
      throws Exception {

    Log.err("Actor: " + this.hashCode() + " POST RESTART"+ self().path());
    super.postRestart(reason);
  }

  /**
   * To be implemented by concrete UntypedActor, this defines the behavior of the
   * UntypedActor.
   *
   * @param message
   */
  @Override public void onReceive(Object message)
      throws Exception {

    if (message instanceof CommunicatorCommand) {
      if(fc == null) {
        fc = Communicator.Instance();
      }
      ++msgCounter;
      CommunicatorCommand cmd = (CommunicatorCommand) message;
      Log.debug("Communicator command:" + cmd.type.name());
      switch (cmd.type) {
        case PING_START:
          FirebaseActor.CommunicatorCommand ping =  new FirebaseActor.CommunicatorCommand(FirebaseActor
                                                                                                     .CommunicatorCommand.Type.PING);
          clusterStatusTick = getContext().system()
                                          .scheduler()
                                          .schedule(Duration.create(PING_DELAY_MS, TimeUnit.MILLISECONDS),
                                                    Duration.create(PING_CYCLE_SEC, TimeUnit.SECONDS),
                                                    getSelf(),
                                                    ping,
                                                    getContext().dispatcher(),
                                                    null);
          break;
        case PING:
          fc.fbServerStatusUpdate();
          if (fc.isLeader()) {
            tripPublisher.processAutoPublishTrips();
            //do trip analysis
            if (ConfigMgr.isProduction()) {
              TripAnalysisActor.Command command = new TripAnalysisActor.Command();
              ActorsHelper.tell(SupervisorActor.UmappedActor.TRIP_ANALYZE, command);
            }
          }
          break;
        case PING_STOP:
          clusterStatusTick.cancel();
          break;
        case LEADER_START:
          FirebaseActor.CommunicatorCommand leader =
              new FirebaseActor.CommunicatorCommand(FirebaseActor
                                                            .CommunicatorCommand.Type.LEADER);
          clusterLeaderTick = getContext().system()
                                          .scheduler()
                                          .schedule(Duration.create(LEADER_DELAY_MS, TimeUnit.MILLISECONDS),
                                                    Duration.create(LEADER_TICK_SEC, TimeUnit.SECONDS),
                                                    getSelf(),
                                                    leader,
                                                    getContext().dispatcher(),
                                                    null);


          CommunicatorToAllActor.Protocol cmdComm2EmailSub = CommunicatorToAllActor.Protocol.build(
              CommunicatorToAllActor.Protocol.Command.SUBSCRIBE);
          ActorsHelper.tell(SupervisorActor.UmappedActor.COMMUNICATOR_TO_ALL, cmdComm2EmailSub);
          break;
        case LEADER:
          fc.clusterKillOrphans();
          driveFlightStatsPoll();
          driveCommunicatorDigest();
          break;
        case LEADER_STOP:
          CommunicatorToAllActor.Protocol cmdComm2EmailUnSub = CommunicatorToAllActor.Protocol.build(
              CommunicatorToAllActor.Protocol.Command.UNSUBSCRIBE);
          ActorsHelper.tell(SupervisorActor.UmappedActor.COMMUNICATOR_TO_ALL, cmdComm2EmailUnSub);
          clusterLeaderTick.cancel();
          break;
        case CHAOS_CONNECT:
          Log.info("CHAOS MODE: CONNECTING....");
          fc.connect();
          int delayDisconnect = fc.clusterChaosScheduleDisconnect();
          Log.info("CHAOS MODE: WILL DISCONNECT IN " + delayDisconnect + " seconds...");
          break;
        case CHAOS_DISCONNECT:
          Log.info("CHAOS MODE: DISCONNECTING....");
          fc.disconnect();
          int delayConnect = fc.clusterChaosScheduleConnect();
          Log.info("CHAOS MODE: WILL CONNECT IN " + delayConnect + " seconds...");
          break;
        case NOTIFICATION_FLIGHT:
          fc.processFlightAlert(cmd.getFlightAlert(), cmd.getFlightAlertEvent());
          break;
        case AKKA_TEST_1:
        case AKKA_TEST_2:
          Log.debug("Actor: " + this.hashCode() +" Got message #" + msgCounter + " type: " + cmd.type.name() +" payload: " + cmd.testMsg + " path: " + getSelf().path());
          break;
      }
    }
    else {
      unhandled(message);
    }
  }

  private void driveFlightStatsPoll() {
    //Only check in production
    if(!ConfigMgr.getInstance().isProd()) {
      return;
    }

    //send to Actor
    final FlightPollAlertsActor.Command cmd = new FlightPollAlertsActor.Command(null);
    ActorsHelper.tell(SupervisorActor.UmappedActor.FLIGHT_POLL, cmd);
  }

  private void driveCommunicatorDigest() {
    CommunicatorDigestActor.Protocol protocol = CommunicatorDigestActor.Protocol.build(
            CommunicatorDigestActor.Protocol.Command.AGENT_DIGEST);
    ActorsHelper.tell(SupervisorActor.UmappedActor.COMMUNICATOR_DIGEST, protocol);

  }
  /**
   * Helper function to send Flight notification to FirebaseActor
   */
  public static void sendCmdNotificationFlight(FlightAlert alert, FlightAlertEvent alertEvent) {
    final CommunicatorCommand cmd = new CommunicatorCommand(alert, alertEvent);
    ActorsHelper.tell(SupervisorActor.UmappedActor.FIREBASE, cmd);
  }


}
