package models.publisher;

import com.avaje.ebean.*;
import com.avaje.ebean.Model;
import com.avaje.ebean.annotation.DbJsonB;
import com.umapped.external.afar.*;
import org.postgis.Point;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.List;
import java.util.Map;
import com.umapped.external.afar.Destination;

/**
 * Created by george on 2017-11-01.
 */
@Entity @Table(name = "afar_country")
public class AfarCountry extends Model implements JsonSupport {

  public static Model.Finder<Integer, AfarCountry> find = new Finder<>(AfarCountry.class);

  @Id
  private int afarId;
  private int afarDestId;
  private String name;
  private String slug;
  private String description;
  private String imageUrl;
  private String photoCredit;
  @Column(name="location")
  private Point location;
  @DbJsonB
  private Map<String, Object> countryLists;
  @DbJsonB
  private Map<String, Object> data;

  public int getAfarId() {
    return afarId;
  }

  public void setAfarId(int afarId) {
    this.afarId = afarId;
  }

  public int getAfarDestId() {
    return afarDestId;
  }

  public void setAfarDestId(int afarDestId) {
    this.afarDestId = afarDestId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getSlug() {
    return slug;
  }

  public void setSlug(String slug) {
    this.slug = slug;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getImageUrl() {
    return imageUrl;
  }

  public void setImageUrl(String imageUrl) {
    this.imageUrl = imageUrl;
  }

  public String getPhotoCredit() {
    return photoCredit;
  }

  public void setPhotoCredit(String photoCredit) {
    this.photoCredit = photoCredit;
  }

  public Point getLocation() {
    return location;
  }

  public void setLocation(Point location) {
    this.location = location;
  }

  public Map<String, Object> getCountryLists() {
    return countryLists;
  }

  public void setCountryLists(Map<String, Object> countryLists) {
    this.countryLists = countryLists;
  }

  public Map<String, Object> getData() {
    return data;
  }

  public void setData(Map<String, Object> data) {
    this.data = data;
  }

  public Map<String, Object> marshalCountryLists(IdList d) {
    return marshal(d);
  }

  public IdList unmarshalCountryLists(Map<String, Object> data) {
    return unmarshal(data, IdList.class);
  }

  public Map<String, Object> marshalData(Destination d) {
    return marshal(d);
  }

  public Destination unmarshalData(Map<String, Object> data) {
    return unmarshal(data, Destination.class);
  }

  public static List<AfarCountry> findCountriesWithPoisByName(String name) {
    com.avaje.ebean.Query<AfarCountryList> countriesWithLists = Ebean
        .createQuery(AfarCountryList.class)
        .select("afarCountryId")
        .where()
        .query();

    return find.where().ilike("name", name).in("afar_id", countriesWithLists).findList();
  }
}
